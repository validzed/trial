// * FUNCTION
// *    => Subprogram yang dipanggil di bagian lain kode kita atau di dalam fungi itu sendiri (rekursi).
// *    => Fungsi dapat menerima dan selalu mengembalikan nilai.
// ?    => Jika tidak mengembalikan nilai, fungsi akan tetap mengembalikan nilai "undefined".
// !    => Fungsi disebut "method" jika menjadi properti "objek"

// * JENIS-JENIS FUNCTION
// *    1. Function Declaration
// ?    2. Function Expression
// ?    3. Named Function Expression
// ?    4. Anonymous Function Expression
// ?    5. Immedietely Invoke Function Expression (IIFE)
// *    6. Arrow Function
// *    7. Named Arrow Function
// *    8. Anonymous Arrow Function
// *    9. Immediately Invoke Arrow Function (IIAF)

// ? Sintaks Function Declaration
function name(param) {
    statement;
}

// * name               : nama fungsi
// * param (optional)   : parameter / argumen
// * statement (optional) : ekspresi, logika, looping, dsb

// ? 1. Function Declaration
function isItEven(num) {
    return num % 2 === 0;
}

console.log(isItEven(10));

// ! Function Declaration di-hoisting. Bisa memanggil fungsi sebelum deklarasi
// ! Sedangkan Arrow Function tidak hoisting
hello();

function hello() {
    console.log('Hello Function!');
}

// * 2. Function Expression
// *    => Sama seperti function declaration
// ! Sintaks:
function name(param) {
    statement;
}
// ? name (optional)
// ? param (optional)
// ? statement (optional)

// * 3. Named Function Expression
// *    => Function Expression yang memiliki nama
// *    => Nama Variable digunakan sebagai nama fungsi
// *    => Nama setelah kata 'function' biasanya tidak ditulis
const isItEven2 = function(num) {
    return num % 2 === 0;
}
console.log(isItEven2(90));

// * 4. Anonymous Function Expression
// *    => Func. Exp. tanpa nama
// *    => Biasanya sebagai callback
// *    => Tidak diinisialisasi ke variable

// * Contoh method 'ForEach'
// *    => ForEach membutuhkan parameter fungsi atau callback yang menerima parameter 'elemen', 'index', dan 'array'.

const arr = [1, 2, 3, 4, 5];
arr.forEach(function(elemen, index, arr) {
    console.log(`Elemen=${elemen} Index=${index} Array=${arr}`);
});

/*  output:
Elemen=1 Index=0 Array=1,2,3,4,5
Elemen=2 Index=1 Array=1,2,3,4,5
Elemen=3 Index=2 Array=1,2,3,4,5
Elemen=4 Index=3 Array=1,2,3,4,5
Elemen=5 Index=4 Array=1,2,3,4,5
*/

// * Kode diatas sama seperti berikut jika menggunakan Named Function Expression
const arr2 = [1, 2, 3, 4, 5];
const logElementAndIndex = function(elemen, index, arr) {
    console.log(`Elemen:${elemen} Indx:${index} Arr:${arr}`);
}
arr2.forEach(logElementAndIndex);

/* output:
ELemen:1 Indx:0 Arr:1,2,3,4,5
ELemen:2 Indx:1 Arr:1,2,3,4,5
ELemen:3 Indx:2 Arr:1,2,3,4,5
ELemen:4 Indx:3 Arr:1,2,3,4,5
ELemen:5 Indx:4 Arr:1,2,3,4,5
*/

// * 5. Immedietely Invoked Function Expression (IIFE)
// *    => Func. Exp. yang langsung dipanggil
// *    => Membutuhkan 2 pasang kurung buka dan tutup berdampingan '(function)(param)'
// !    => kurung pertama (function) = mendeklarasikan Anonymous Function Exp.
// !    => kurung kedua (param) = memanggil fungsi tersebut, bisa memiliki parameter ataupun tidak

// * Contoh IIFE tanpa parameter:
(function() {
    console.log('Hello IIFE non parameter!');
})(); // Hello IIFE non parameter!

// * IIFE dengan parameter
(function(num) {
    console.log(`Angka ${num}`);
})(5); // Angka 5


// * 6. Arrow Function
// ?    => mirip Function Expression
// ?    => Tidak memerlukan kata 'function'
// ?    => Tanda panah (=>) diantara 'kurung lengkung' dan 'kurung kurawal'
// ! Arrow Function tidak hoisting
// ! Sintaks:
(param) => {
    statement;
};

// * param (opt) = jika hanya 1 param, kurung lengkung dapat dihilangkan
// * statements (opt) = stat atau pernyataan termasuk ekspresi, logika, looping, dsb. 

// * 7. Named Arrow Function
// *    => biasanya diinisialisasi ke variable
// *    => nama variable = nama fungsi
// ! Contoh:
const isItEven3 = (num) => {
    return num % 2 === 0;
};
console.log(isItEven(6)); // true
console.log(isItEven3(9)); // false

// ! Jika 1 param, kurung lengkung dapat dihilangkan
const isItEven4 = num => {
    return num % 2 === 0;
}
console.log(`${isItEven(470)} ==> LINE 139`); // true

// ! karena hanya 1 ekspresi dan 1 statement, kita bisa menghapus 'return' dan 'kurung kurawal' {...}
const isItOdd = (num) => num % 2 === 1;
console.log(`${isItOdd(7)} ==> LINE 143`); // true
// ! Note: hanya 1 statement

// * 8. Anonymous Arrow Function
// *    => Tanpa nama, biasanya menggunakan 'callback'

const arr3 = [2, 1, 5, 3, 4];
arr3.forEach((elemen) => {
    console.log(`Elemen: ${elemen}`);
});

// ! Kode diatas sama jika menggunakan Named Arrow Function
const arr4 = [2, 1, 5, 3, 4];
const logElement = (elemen) => {
    console.log(`Elemen=${elemen}`);
}
arr4.forEach(logElement);

// * 9. Immedietely Invoked Arrow Function (IIAF)
// *    => langsung dipanggil
// ! Contoh:
(() => {
    console.log('Hello world ==> LINE 164');
})();

// ! Dengan parameter
((nama) => {
    console.log(`Hello ${nama}`);
})('Reno');