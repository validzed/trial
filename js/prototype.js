// ! Prototype = konsep fundamental JavaScript
// * JS adalah bahasa asynchronous, non-blocking, dan prototype-based.
// * Di JS, Object sangat penting.
// * Object dapat disimpan dalam data structure, dijadikan argumen, atau return value dari function lain.

// TODO: contoh pertama
// TODO: https://medium.com/koding-kala-weekend/memahami-javascript-sebagai-prototype-based-language-4ea7d18590e7
var o = {
    a: 2,
    m: function() {
        return this.a + 1;
    }
}

console.log(o.a); // 2

// ? it return statement that this object is a function
console.log(o.m); // [function: m] atau function dianggap objek biasa

console.log(o.m()); // 3
// * Poinnya adalah, sebuah function dianggap belum aktif jika ditulis tanpa ();

// * Dalam JS juga tidak mengenal konsep class, maka inheritance (pewarisan) menggunakan property khusus yang dimiliki semua objek yaitu "prototype"

// ! ====================================
// ! __proto__ Property dan Object.create
// *    => mengakses __proto__ property secara langsung sangat tidak disarankan karena berdampak pada performance
// *    => contoh berikut untuk memudahkan pemahaman tentang inheritance

// TODO: Contoh __proto__ #1
var bird = {
    fly: true,
    color: 'blue'
};

var eagle = {};

eagle.__proto__ = bird;

// ? Object 'eagle' mewarisi semua property object 'bird'
console.log(eagle.fly); // true
console.log(eagle.color); // blue

// TODO: Contoh __proto__ #2
var x = {
    a: 1,
    m: function(c) {
        return this.a + this.b + c;
    }
};

var y = {
    b: 2,
    __proto__: x
}

var z = {
    __proto__: y
}

console.log(y.m(7) + " ==> LINE 61"); // 10
console.log(z.m(17) + " ==> LINE 62"); // 20

// TODO: Contoh Object.create #3
var bird2 = {
    fly: true,
    color: 'brown',
    a: 1,
    m: function(c) {
        return this.a + this.b + c;
    }
};

var eagle2 = Object.create(bird2);
console.log(eagle2.fly + " ==> LINE 75");
console.log(eagle2.color);

var ye = Object.create(bird2, { b: { value: 2 } });

var ze = Object.create(ye);

console.log(ze.m(7)); // 10
console.log(ye.m(17)); // 20

// * Script diatas sama dengan contoh menggunakan __proto__
// * Perlu dicatat: perubahan state yang terjadi di parent menyebabkan state yang ada di child berubah,
// * sementara perubahan state di child tidak berpengaruh pada parent

var parent = {
    hair: 'curly',
    eyes: 'blue'
};

var children = Object.create(parent);

console.log(`Parent hair: ${parent.hair}, parent eyes: ${parent.eyes}`);
console.log(`Children hair: ${children.hair}, children eyes: ${children.eyes}`);

// ? set parent hair to straight
parent.hair = 'straight';

console.log(`Parent hair: ${parent.hair}`); // straight
console.log(`Children hair: ${children.hair}`); // straight

// ? set children eyes
children.eyes = 'green';
console.log(`Parent eyes: ${parent.eyes}`); // blue
console.log(`Children eyes: ${children.eyes}`); // green

function Bird() {
    this.fly = true;
}
// ! code Bird() = Bird2
class Bird2 {
    constructor() {
        this.fly = true;
    }
}

Bird.prototype.color = 'brown';
Bird2.prototype.color = 'cokelat';

var eagle3 = new Bird();

console.log(`${eagle3.fly} ${eagle3.color}`);