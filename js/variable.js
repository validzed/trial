// ! Var    = 1. Reassigned (Perubahan Nilai)
// !          2. Redeclared (Deklarasi Ulang)
// !          3. Hoisting (Diangkat)
// !          4. Function Scope

// * Let    = 1. Reassigned
// *          2. Block Scope
// *          3. Function Scope

// ? Const  = 1. Block Scope
// ?          2. Function Scope

// * BLOCK SCOPE
// *    => Kode di dalam lingkup Conditional Stat (if, else, else if), Loooping (for, while, do while), Switch Stat, dsb selain lingkup 'Function' atau 'Global'
// *    => Tidak dapat diakses di luar block scope {...}


// ? FUNCTION SCOPE
// ?    => Kode di dalam lingkup function (fungsi) antara {...}
// ?    => Tidak dapat diakses di luar function (fungsi)

// ! =========================================

// * variable namePerson is hoisting
console.log(namePerson); // undifined

// * global scope
var namePerson = 'Reza'
if (true) {
    // *block scope
    var namePerson = 'Ucup'
}

// * global scope 
console.log(namePerson); // Ucup