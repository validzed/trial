// *    => https://www.w3schools.com/js/js_this.asp
// *    => Kata kunci this mengacu pada suatu objek
// *    => 'this' mengacu pada objek yang berbeda tergantung cara penggunaannya

// *    => 'this' bukan variable. 'this' adalah 'keyword'. 
// *    => Nilai 'this' tidak bisa dirubah.


// * 1) 'this' dalam Method
// *    => mengacu kepada Object
// TODO: contoh berikut mengacu kepada Objek person
const person = {
    firstName: "John",
    lastName: "Doe",
    id: 5566,
    fullName: function() {
        // * this mengacu kepada objek person
        return this.firstName + " " + this.lastName;
    },
};
console.log(person.fullName()); // John Doe

// * 2) Ketika 'this' digunakan sendiri
// *    => mengacu kepada Global Object
// *    => karena berjalan di Global Scope
let x = this;
// document.getElementById('demo').innerHTML = x; // [object Window]

// ! Dalam 'strict mode', juga mengacu Global Object
"use strict";
let y = this;
// document.getElementById('demo').innerHTML = y; // [object Window]

// * 3) 'this' dalam Function (default)
// *    => dalam function, secara default 'this' adalah Global object
// *    => pada browser, Global object adalah [object Window]

function myFunction() {
    return this;
}
// console.log(myFunction());

// * 4) 'this' dalam Function (strict)
// *    => tidak mengizinkan pengikatan default
// *    => jadi ketika digunakan, 'this' menjadi 'undefined'

"use strict";

function myFunction2() {
    return this;
}

// console.log(myFunction2());

// * 5) 'this' dalam Event Handlers
// *    => pada HTML event handlers, 'this' merujuk pada Elemen HTML

/* Example : 
<button onclick = "this.style.display='none'" >
    Click to Remove Me!
</button>
*/

// * 6) Explicit Function Binding
// *    => method call() dan apply() = method JavaScript yang telah ditentukan sebelumnya
// *    => keduanya dapat digunakan untuk memanggil 'method objek' dengan 'objek lain' sebagai argumen

// TODO: Example
const person1 = {
    fullName() {
        return this.firstName + ' ' + this.lastName;
    }
}

const person2 = {
    firstName: 'Dodi',
    lastName: 'Sunaryo'
}

let z = person1.fullName.call(person2);
console.log(z); // Dodi Sunaryo

// * 7) Function Borrowing (fungsi peminjaman)
// *    => dengan method bind(), suatu objek dapat 'meminjam method' objek lain
// TODO: Contoh berikut, 'member' meminjam method fullName dari 'person'
const person3 = {
    firstName: "Deni",
    lastName: 'Cagur',
    fullName: function() {
        return this.firstName + ' ' + this.lastName;
    }
}

const member3 = {
    firstName: 'Hege',
    lastName: ' Nilsen'
}

let borrowFullName = person3.fullName.bind(member3); // ! meminjam method
console.log(borrowFullName()); // Hege Nilsen

let callFullName = person3.fullName.call(member3); // ! memanggil method
console.log(callFullName); // Hege Nilsen

// * 8. 'this' pada Arrow Function
// *    => berbeda dengan Regular Function

// * 'this' pada "Regular Function" mewakili objek, tergantung bagaimana fungsi dipanggil, bisa berupa Window, Document, Button, atau apapun 

// * 'this' pada "Arrow Function" mewakili objek yang memiliki fungsi tersebut, tidak peduli siapa yang memanggil fungsi tersebut

// ! berikut 'this' kembali kepada Window kemudian Button karena dia yang memanggil
hello = function() {
    document.getElementById("demo").innerHTML += this;
}

// ? The windows object calls the function
window.addEventListener("load", hello); // * [object Window]

// ? A button object calls the function
document.getElementById("btn").addEventListener("click", hello); // * [object HTMLButtonElement]

// ! Arrow Function =========================================
hello2 = () => {
    document.getElementById("demo").innerHTML += this;
}

// ? The window object calls the function
window.addEventListener("load", hello); // * [object Window]

// ? A button object calls the function
document.getElementById("btn").addEventListener("click", hello); // * [object Window]