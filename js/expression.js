// * STATEMENT
// *    => unit sintaks yang menyatakan aksi atau tugas untuk dilakukan.

// ? EKSPRESI
// ?    => entitas sintaks yang menghasilkan sebuah nilai.


// todo: Statement
var x = 1; // * statement membuat variabel x
console.log("Hello world!"); // * statement menampilkan teks

// todo: Ekspresi
x + 1; // * ekspresi menjumlahkan x dengan 1

// todo: Statement yang di dalamnya terdapat Ekspresi
var x = 4 + 2;

// * Apabila bisa disimpan dalam variabel maka itu Ekspresi
// * Jika tidak, maka Statement
// todo: Contoh
var x = 4 * 2; // ? 4 * 2 adalah Ekspresi
// var z = if (x > 10) { z = 10 } // ? error, if merupakan statement

// * Ekspresi dalam String Interpolation
console.log(`Sebuah string ${1 + 2}`);

// * Jika ekspresi pada String Interpolation tidak error, maka termasuk Ekspresi
// * Jika error, maka Statement
// ! contoh salah
// ? console.log(`Sebuah string ${if(true){alert("Hi")}}`);

// * if(true){alert("Hi")} adalah statement, karena tidak dapat ditulis dalam string interpolation