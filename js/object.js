// ! Di JavaScript, Object are King. Jika paham Object, Anda paham JavaScript

// ! Di JavaScript, hampir semuanya Object
// * 1. Booleans dapat menjadi objects (jika difenisikan dengan keyword 'new')
// * 2. Numbers dapat menjadi objects (jika difenisikan dengan keyword 'new')
// * 3. Strings dapat menjadi objects (jika difenisikan dengan keyword 'new')
// * 4. Dates selalu object
// * 6. Regular Expressions selalu object
// * 5. Maths selalu object
// * 7. Arrays selalu object
// * 8. Functions selalu object
// * 9. Objects selalu object

// ! Semua nilai JavaScript kecuali primitive adalah objects

// * Di JavaScript, Object adalah kumpulan nilai bernama (named values)

// ! Nilai bernama disebut Properties
// ! Property:      Value:
// ! firstName      John
// ! age            50
// ! fullName       function() {...}

// * Properti objek dapat berupa: nilai primitif / non primitif, objek lain, dan fungsi

// ? OBJECT juga Variable yang memiliki banyak nilai
// * Bedanya fungsi dengan method? Jika fungsi berada di dalam fungsi, dia menjadi method. Sebenarnya fungsi = method

// ! Primitive values are Immutable
// ! Nilai primitif tidak dapat berubah
// ! Jika x = 3,14 Anda dapat mengubah nilai x, tapi Anda tidak dapat mengubah nilai 3,14
// todo: https://www.w3schools.com/js/js_object_definition.asp

// * JavaScript Objects are Mutable
// * Objek bisa berubah: mereka ditangani dengan referensi, bukan berdasarkan nilai
// * Jika 'person' adalah object, statement berikut tidak akan meng-copy 'person'
// const x = person; // * will not create copy of person

// * Objek x bukan salinan 'person'. x dan person adalah objek yang sama. 
// * Setiap perubahan x juga akan mengubah person, karena mereka objek yang sama.

const person = {
    firstName: 'John',
    lastName: 'Doe',
    age: 50,
    eyeColor: 'blue'
}

const x = person;
x.age = 10; // * will change both x.age and person.age


const car = {
    // ? property
    carName: 'Ferari',
    carType: 'GTC4Lusso',
    carPrice: 12000,

    // ? method / function expression
    start: function() {
        return 'started';
    },

    // ? method / anonymous arrow function
    stop: () => {
        return 'stoped';
    },

    // ? method / function declaration
    getName() {
        return this.carName;
    }
};

console.log(car.getName());
console.log(car.start());
console.log(car.stop());