// * Operator = Simbol operasi
// * Operan = Nilai atau Variabel yang melakukan operasi

// ! OPERATOR ARITMATIKA
// ! **=    Exponensial / Pangkat
// ! ++     Increment
// ! --     Decrement

// ? OPERATOR PENUGASAN
// ? =      Ex:  x = y    Same as:  x = y
// ? +=     Ex:  x += y   Same as:  x = x + y
// ? **=    Ex:  x **= y  Same as:  x = x ** y

// * OPERATOR LOGIKA
// * &&     And (dan)
// * ||     Or (atau)
// * !      Not (tidak)

// ? OPERATOR PERBANDINGAN
// ? ==     Nilai sama dengan
// ? ===    Nilai dan Tipe Data sama dengan

// * OPERATOR TERNARY
// *    => mempersingkat conditional statement (if/else)
// *    => kondisi biasanya berupa Boolean, jika bukan Boolean akan dipaksa menjadi Boolean
// *    => Sintaks: kondisi ? jikaBenar : jikaSalah

// ? OPERATOR Typeof
// ?    => mengecek tipe data atau jenis objek suatu nilai

const isTrue = true;
const isFalse = false;
if (isTrue || isFalse) {
    console.log("Hello world! => LINE 34"); // no output
}

if (isTrue && isTrue) {
    console.log("Hello world! => LINE 38"); // Hello world! => LINE 30
}

if (isFalse && isFalse) {
    console.log("Hello world! => LINE 42"); // no output
}

console.log(!isTrue); // * false

// Aritmatika
let exponential = 2 ** 3; // * 2 pangkat 3 = 8
console.log(exponential);

// Typeof
console.log(typeof ""); // string
console.log(typeof 1); // number
// console.log(typeof 2n); // bigint
console.log(typeof null); // object
console.log(typeof undefined); // undefined
console.log(typeof {}); // object
console.log(typeof []); // object