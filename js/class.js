// * Class adalah template untuk objek JavaScript
// * selalu tambahkan method constructor():
// ! Syntax:
class ClassName {
    constructor() {}
}

// Example
class Car {
    constructor(nameCar, yearCar) {
        this.nameCar = nameCar;
        this.yearCar = yearCar;
    }
}

// ! class bukanlah object, class hanyalah template
let mobil = new Car("Toyoga", "2022");
// * contoh diatas menggunakan kelas Car untuk membuat 2 objek Car

// ! ==============================================
// * METHOD CONSTRUCTOR
// *    => method constructor adalah method khusus
// *    => dieksekusi otomatis ketika Objek baru dibuat
// *    => digunakan untuk "menginisiaisasi properti objek"
// !    => Jika tidak mendefenisikan 'constructor', JavaScript akan menambahkan method 'constructor' kosong

// * CLASS METHOD
// *    => 'class methods' dibuat dengan sintaks yang sama dengan 'object methods'
// ! Syntax:
class ClassName2 {
    constructor() {}
    method_1() {}
    method_2() {}
    method_3() {}
}

// ! Example Class Method 1
class Car2 {
    constructor(name, year) {
        this.name = name;
        this.year = year;
    }
    age() {
        let date = new Date();
        return date.getFullYear() - this.year;
    }
}

let myCar = new Car2("Hendi", 1997);
console.log(`${myCar.age()} ==> LINE 50`);

// ! bisa juga dengan parameter
class Car3 {
    constructor(name, year) {
        this.name = name;
        this.yearCar = year;
    }
    age(x) {
        return x - this.yearCar;
    }
}

let myCar3 = new Car3("Mitsubima", 2000);
console.log(`${myCar3.age(2022)} ==> LINE 65`);

// ! "use strict"
// *    => Sintak dalam class harus ditulis dalam "strict mode" (mode ketat)
// *    => "strict mode" akan menyebabkan error jika menggunakan variable tanpa men-deklarasi-kan

class Car4 {
    constructor(name, year) {
        this.nameCar = name;
        this.yearCar = year;
    }
    age() {
        // ! date = new Date(); // this will not work
        let date = new Date(); // * this will work
        return date.getFullYear() - this.yearCar;
    }
}

class Human {
    constructor(name, address) {
        this.humanName = name;
        this.humanAddress = address;
    }

    introduce() {
        console.log(`Hi, my name is ${this.humanName}`);
    }

    work() {
        console.log('Work!');
    }
}

// Add prototype/instance method
Human.prototype.greet = function(name) {
    console.log(`Hi, ${name}, I'm ${this.humanName}`);
}

// Add static method
Human.destroy = function(thing) {
    console.log(`Human is destroying ${thing}`);
}

class Programmer extends Human {
    constructor(name, address, language) {
        super(name, address);
        // ? call the super/parent class constructor, in this case Human.constructor
        this.programmingLanguage = language;
    }

    // * Override the Instance Method (tidak mengubah parameter)
    // introduce() { // tidak mengubah parameter dari introduce() parent
    //     super.introduce();
    //     // ? call the super class introduce instance method
    //     console.log(`I can write, ${this.programmingLanguage}`);
    // }

    // * Overload the Introduce Method (mengubah parameter)
    introduce(withDetail) { // ? menambah parameter withDetail padahal 'introduce() parent' tidak ada parameter
        super.introduce(); // ? call the super introduce instance method
        (Array.isArray(withDetail)) ? console.log(`I can write ${this.programmingLanguage}`): console.log("Wrong input");
    }

    code() {
        let acak = Math.floor(Math.random() * this.programmingLanguage.length);
        console.log("Code some", this.programmingLanguage[acak]);
    }
}

let Obama = new Human("Barrack Obama", "Washington DC");
Obama.introduce(); // Hi, my name is Barrack Obama

let Isyana = new Programmer("Isyana", "Jakarta", ["JavaScript", "Kotlin", "Python"]);
Isyana.introduce(["java"]);
// * output :
// * Hi, my name is Isyana
// * I can write, JavaScript,Kotlin,Phyton

Isyana.code(); // ? Code some Javascript/Kotlin/Phyton
Isyana.work(); // ? call super class method that isn't overrided or overload

// * console.log(Obama instanceof Human); // true
// * console.log(Obama instanceof Programmer); // false
// * console.log(Isyana instanceof Programmer); // true

Obama.greet("Donald Trump");
Human.destroy("Amazon Forest");