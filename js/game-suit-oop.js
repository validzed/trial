class TrialGame {
    constructor() {
        this.playerName = 'PLAYER 1';
        this.compName = 'COMP';
        this.playerChoosen;
        this.compChoosen;
        this.winner;
        this.playerScore = 0;
        this.compScore = 0;
    }

    set setPlayerChoosen(choosen) {
        this.playerChoosen = choosen;
    }

    get getPlayerChoosen() {
        return this.playerChoosen;
    }

    set setCompChoosen(choosen) {
        this.compChoosen = choosen;
    }

    get getCompChoosen() {
        return this.compChoosen;
    }

    compBrain() {
        const option = ['scissor', 'rock', 'paper'];
        const comp = option[Math.floor(Math.random() * option.length)];
        return comp;
    }

    gameRules() {
        if (this.playerChoosen == this.compChoosen) return this.winner = 'DRAW';
        if (this.playerChoosen == 'scissor') return (this.compChoosen == 'rock') ? this.winner = this.compName : this.winner = this.playerName;
        if (this.playerChoosen == 'rock') return (this.compChoosen == 'paper') ? this.winner = this.compName : this.winner = this.playerName;
        if (this.playerChoosen == 'paper') return (this.compChoosen == 'scissor') ? this.winner = this.compName : this.winner = this.playerName;
    }

    gameResult() {
        if (this.winner != 'DRAW') return `${this.winner} WIN`;
        return `${this.winner}`;
    }

    countScore() {
        if (this.winner == this.compName) return this.compScore += 1;
        if (this.winner == this.playerName) return this.playerScore += 1;
    }

    showScore() {
        console.log(`Player Score = ${this.playerScore} VS Comp Score = ${this.compScore}`);
    }
}

const start = new TrialGame();

function pChoosen(option) {
    // document.getElementById('player-choosen').innerHTML = 'player: ' + option;
    // const start = new TrialGame();

    start.setPlayerChoosen = option;
    start.setCompChoosen = start.compBrain();
    start.gameRules();
    start.countScore();

    // document.getElementById('comp-choosen').innerHTML = 'comp: ' + start.getCompChoosen;

    // ? player background css
    document.querySelector(`.player-choosen #${start.getPlayerChoosen}`).style.backgroundColor = 'aqua';

    let result = document.getElementById('game-result');

    // ? comp background css
    setTimeout(() => {
        result.style.display = 'block';
        result.innerHTML = start.gameResult();
        document.querySelector(`.comp-choosen #${start.getCompChoosen}`).style.backgroundColor = 'aqua';
    }, 500);
    // document.querySelector(`.comp-choosen #${start.getCompChoosen}`).style.backgroundColor = 'aqua';

    // ? disable pointer
    let pointer = document.querySelectorAll('.player-choosen img, .comp-choosen img');
    pointer.forEach((elemen) => {
        elemen.style.pointerEvents = 'none';
    });

    // console.log(start.gameResult());
    // console.log(start.countScore());
    start.showScore();
}

function refreshGame() {
    // let player = document.querySelectorAll('.player-choosen img');
    // player.forEach((elemen) => {
    //     elemen.style.backgroundColor = '';
    // });

    // let comp = document.querySelectorAll('.comp-choosen img');
    // comp.forEach((elemen) => {
    //     elemen.style.backgroundColor = '';
    // });

    document.getElementById('game-result').style.display = 'none';
    // document.getElementById('scissor').style.backgroundColor = '';

    // ? enable pointer
    let refresh = document.querySelectorAll('.player-choosen img, .comp-choosen img');
    refresh.forEach((elemen) => {
        elemen.style.pointerEvents = 'auto';
        elemen.style.backgroundColor = '';
    });

    // localStorage.clear();
    // sessionStorage.clear();
}


// const start = new TrialGame();

// start.setPlayerChoosen = 'scissor';
// start.setCompChoosen = start.compBrain();
// console.log(`Player choose: ${start.getPlayerChoosen}`);
// console.log(`Comp choose: ${start.getCompChoosen}`);
// console.log(`Result: ${start.gameRules()}`);