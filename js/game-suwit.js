const playerChoose = 'paper';
let compChoose = Math.floor(Math.random() * 3);

if (compChoose == 0) {
    compChoose = 'scissor';
} else if (compChoose == 1) {
    compChoose = 'rock';
} else {
    compChoose = 'paper';
}

let gameResult = null;
if (playerChoose == compChoose) {
    gameResult = 'DRAW!';
} else if (playerChoose == 'scissor') {
    (compChoose == 'paper') ? gameResult = 'PLAYER WIN!': gameResult = 'COMP WIN!';
} else if (playerChoose == 'rock') {
    (compChoose == 'paper') ? gameResult = 'COMP WIN!': gameResult = 'PLAYER WIN!';
} else if (playerChoose == 'paper') {
    (compChoose == 'scissor') ? gameResult = 'COMP WIN!': gameResult = 'PLAYER WIN!';
}

console.log(`Player choose: ${playerChoose}`);
console.log(`Comp choose: ${compChoose}`);
console.log(`Result: ${gameResult}`);