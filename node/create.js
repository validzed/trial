const fs = require('fs')

const createPerson = person => {
    fs.writeFileSync('./person.json', JSON.stringify(person))
    return person
}

createPerson({
    name: 'Sabrina',
    age: 22,
    address: 'BSD'
})